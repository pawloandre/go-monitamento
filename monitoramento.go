package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const MONITORAMENTOS = 3
const ESPERA = 2

func main() {

	exibeIntroducao()

	for {

		exibeMenu()
		comando := leComando()

		switch comando {

		case 1:
			iniciaMonitora()
		case 2:
			imprimeLogs()
		case 0:
			fmt.Println("Saindo programa...")
			os.Exit(0)
		default:
			fmt.Println("Comando inválido!")
			os.Exit(-1)
		}

	}

}

func exibeIntroducao() {
	nome := "Paulo"
	versao := 1.0

	fmt.Println("Bem-vindo Sr.", nome)
	fmt.Println("A versão desta aplicação é", versao)
	fmt.Println("")
}

func exibeMenu() {
	fmt.Println("1 - Iniciar monitoramento")
	fmt.Println("2 - Exibir Logs")
	fmt.Println("0 - Sair do Programa")
	fmt.Println("")
}

func leComando() int {
	comando := 1
	fmt.Scan(&comando)
	return comando
}

func iniciaMonitora() {
	fmt.Println("Iniciando monitoramento...")

	sites := leSitesDoArquivo()

	count := 0

	for count < MONITORAMENTOS {

		for _, site := range sites {
			fmt.Println("Testando o site", site)
			monitoraSite(site)
		}

		fmt.Println("")
		time.Sleep(ESPERA * time.Second)
		count++

	}
}

func monitoraSite(site string) {
	resposta, erro := http.Get(site)

	if erro != nil {
		fmt.Println(erro)
	}

	statusCode := resposta.StatusCode

	if statusCode == 200 {
		fmt.Println("Site:", site, "foi carrgeado com sucesso")
		registraLog(site, true)
	} else {
		fmt.Println("Site:", site, "está com problemas. Status Code:", statusCode)
		registraLog(site, false)
	}
}

func exibeLogs() {
	fmt.Println("Exibindo logs...")
}

func leSitesDoArquivo() []string {

	var sites []string

	arquivo, erro := os.Open("sites.txt")

	if erro != nil {
		fmt.Println(erro)
	}

	leitor := bufio.NewReader(arquivo)
	for {
		linha, err := leitor.ReadString('\n')
		linha = strings.TrimSpace(linha)

		sites = append(sites, linha)

		if err == io.EOF {
			break
		}

	}

	arquivo.Close()
	return sites
}

func registraLog(site string, status bool) {

	arquivo, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println(err)
	}

	arquivo.WriteString(time.Now().Format("02/01/2006 15:04:05") + " - " + site + " - online: " + strconv.FormatBool(status) + "\n")

	arquivo.Close()
}

func imprimeLogs() {

	arquivo, err := ioutil.ReadFile("log.txt")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(arquivo))

}
